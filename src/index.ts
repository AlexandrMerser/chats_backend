const express = require('express')
const app = express()

app.get('/', (_req: any, res: any) => {
    res.send('Hello World!')
})

app.listen(8080, () => {
    console.log(`Example app listening at http://localhost:${8080}`)
})